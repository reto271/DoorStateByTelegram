#!/usr/bin/env python3
# encoding: utf-8

# Libraries
import os
import unittest
import xmlrunner
import shutil
import sys

if __name__ == '__main__':
    root_dir = os.path.dirname(__file__)
    try:
        shutil.rmtree(root_dir + '/test-reports')
    except:
        pass
    test_loader = unittest.TestLoader()
    package_tests = test_loader.discover('.', pattern='Test_*.py')

    testRunner = xmlrunner.XMLTestRunner(output='test-reports')
    feedback = testRunner.run(package_tests)
    posError = str(feedback).find("errors=0")
    posFailure = str(feedback).find("failures=0")
    if (-1 == posError) or (-1 ==posFailure):
        sys.exit(1)
    else:
        sys.exit(0)