# Libraries

# My modules
import myUtils

# ------------------------------------------------------------------------------
# Register Users, the admin shall aprove new users.
class AccesRequestHandler:

    def __init__(self, debugg_logger, user_access_list, user_notification_list, bot):
        self.m_adminId = 0
        self.m_pendingReqList = []
        self.debug_logger = debugg_logger
        self.user_access_list = user_access_list
        self.user_notification_list = user_notification_list
        self.bot = bot

    def initialize(self):
        try:
            with open('./cnfg/adminId.txt', 'r') as idfile:
                self.m_adminId = myUtils.tryInt(idfile.read().rstrip())
                self.debug_logger.logText('Admin Id: ' + str(self.m_adminId))
        except IOError:
            self.debug_logger.logText('Admin not yet defined.')

    def requestPermission(self, newUserFirstName, newUserLastName, newUserName, newUserId):
        if 0 == self.m_adminId:
            self.debug_logger.logText('admin not yet defined...')
            self.setNewAdmin(newUserId)
            self.user_access_list.addUser(newUserId)
            self.user_access_list.storeList()
            self.user_notification_list.addUser(newUserId)
            self.user_notification_list.storeList()
        else:
            self.debug_logger.logText('admin already defined...')
            self.sendRequestToAdmin(newUserFirstName, newUserLastName, newUserName, newUserId)
            self.addRequestToList(newUserId)

    def setNewAdmin(self, newUserId):
        with open('./cnfg/adminId.txt', 'w') as f:
            f.write(str(newUserId) + '\n')
            self.m_adminId = newUserId
            self.debug_logger.logText('New registered admin: ' + str(newUserId))
            self.bot.sendMessage(self.m_adminId, 'You are registered as admin')

    def sendRequestToAdmin(self, newUserFirstName, newUserLastName, newUserName, newUserId):
        reqText = 'User [' + newUserFirstName + ' ' + newUserLastName + ' ' + newUserName + '] (ID: ' + str(newUserId) + ') requests access.'
        self.bot.sendMessage(self.m_adminId, reqText)
        self.debug_logger.logText(reqText)

    def addRequestToList(self, newUserId):
        self.m_pendingReqList.append(newUserId)

    def ackNewUser(self, newUserId):
        newUserIdInt = myUtils.tryInt(newUserId)
        if True == self.isFeedbackCorrect(newUserIdInt):
            self.m_pendingReqList.remove(newUserIdInt)
            self.user_access_list.addUser(newUserIdInt)
            self.user_access_list.storeList()
            self.user_notification_list.addUser(newUserIdInt)
            self.user_notification_list.storeList()
            ackText = 'Your request was approved.'
            self.bot.sendMessage(newUserIdInt, ackText)
            self.debug_logger.logText(ackText + ' (' + newUserId + ')')

    def rejectNewUser(self, newUserId):
        newUserIdInt = myUtils.tryInt(newUserId)
        if True == self.isFeedbackCorrect(newUserIdInt):
            self.m_pendingReqList.remove(newUserIdInt)
            rejectText = 'Your request was rejected.'
            self.bot.sendMessage(newUserIdInt, rejectText)
            self.debug_logger.logText(rejectText + ' (' + newUserId + ')')

    def isFeedbackCorrect(self, newUserId):
        requestFound = False
        for user in self.m_pendingReqList:
            if user == newUserId:
                requestFound = True
        if False == requestFound:
            respText = 'No request pending to req: ' + str(newUserId)
            self.debug_logger.logText(respText)
            self.bot.sendMessage(self.m_adminId, respText)
        return requestFound

    def showPendingRequests(self):
        testPendingReq = 'Pending req:\n'
        self.debug_logger.logText('Pending Requests >>>')
        for req in self.m_pendingReqList:
            self.debug_logger.logText(str(req))
            testPendingReq = testPendingReq + str(req) + '\n'
        self.debug_logger.logText('Pending Requests <<<')
        self.bot.sendMessage(self.m_adminId, testPendingReq)

    def isAdmin(self, userId):
        retValue = False
        self.debug_logger.logText('isAdmin')
        if userId == self.m_adminId:
            retValue = True
        else:
            responseText = 'Command requires admin previdges'
            self.debug_logger.logText(responseText)
            self.bot.sendMessage(self.m_adminId, responseText)
        return retValue