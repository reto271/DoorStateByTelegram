# Libraries
import datetime
from enum import Enum

# My modules
from TimeHandler import TimeHandler


# ------------------------------------------------------------------------------
class DoorState(Enum):
    """
    Enumerate the door state.

    Need a closing state to not multiple triggering
    a door action
    """

    NOT_INITIALIZED = 0
    CLOSED = 1
    OPEN = 2
    CLOSING_COMMAND_SENT = 3


# ------------------------------------------------------------------------------
class AutomaticDoorClosing:
    """
    Close the door automatically if open during the night.
    The door state supervisor monitores the state of the door. If the door is
    open for more than 10 minutes during the night, it will be automatically
    closed. Night = 2200 - 0600
    """

    # ------------------------------------------------------------------------------
    def __init__(self, input_handler, output_pulse_handler, time_handler, debug_logger):
        """
        Construct the door state supervisor.

        Parameters
        ----------
        input_handler : binary input
            To monitor the current door state.
        output_pulse_handler : output pulse handler
            Generates output pulses to trigger a door movement
        debug_logger : logger
            Log messages to the debug log file

        Returns
        -------
        None.
        """
        self.input_handler = input_handler
        self.output_pulse_handler = output_pulse_handler
        self.debug_logger = debug_logger
        self.state = DoorState.NOT_INITIALIZED
        self.autoClosingAtNightEnabled = True
        self.time_handler = time_handler

        # Set initial state of FSM
        if self.input_handler.getState():
            self.state = DoorState.OPEN
        else:
            self.state = DoorState.CLOSED
        self.last_change_time = self.time_handler.get_current_time()
        self.debug_logger.logText("AutomaticDoorClosing started")

    # ------------------------------------------------------------------------------
    def process(self):
        """
        Process function of the state machine.

        Returns
        -------
        string
            [] if no action was commanded, returns a text if the door is
            automatically closed. The test is may be sent to the chat bot.
        """

        self._eventuallyReenableAutomaticClosing()

        if self.state == DoorState.CLOSED:
            self._state_door_closed()
        elif self.state == DoorState.OPEN:
            return self._state_door_open()
        elif self.state == DoorState.CLOSING_COMMAND_SENT:
            self._state_door_auto_close_command_sent()
        else:
            assert(False)
        return []

    # ------------------------------------------------------------------------------
    def disableAutoClosingToday(self):
        """Disable automatic closing for today."""
        self.autoClosingAtNightEnabled = False

    # ------------------------------------------------------------------------------
    def enableAutoClosingToday(self):
        """Enable automatic closing for today."""
        self.autoClosingAtNightEnabled = True

    # ------------------------------------------------------------------------------
    def isAutoClosingEnabled(self):
        """ Return true if the automatic door closing during the night is enabled.

        Returns
        -------
        Bool
            True if enabled.
        """
        return self.autoClosingAtNightEnabled

    # ------------------------------------------------------------------------------
    def _state_door_closed(self):
        """
        Process door state closed.

        Internal state: door is closed, wait for opening event.

        Returns
        -------
        None.
        """
        current_door_state_open = self.input_handler.getState()
        if current_door_state_open:
            self.last_change_time = self.time_handler.get_current_time()
            self.state = DoorState.OPEN
            self.debug_logger.logText("AutomaticDoorClosing: new state: OPEN")

    # ------------------------------------------------------------------------------
    def _state_door_open(self):
        """
        Process door state open.

        Internal state: door is open. Wait either for closing or monitor the
        daytime and door opening time to automatically close it.

        Returns
        -------
        string
            String with message to send to the chat bot if the autoclosing was
            triggered, otherwise [] is returned
        """
        current_door_state_open = self.input_handler.getState()
        if not current_door_state_open:
            self.last_change_time = self.time_handler.get_current_time()
            self.state = DoorState.CLOSED
            self.debug_logger.logText("AutomaticDoorClosing: new state: CLOSED")
        else:
            if self.autoClosingAtNightEnabled:
                time_now = self.time_handler.get_current_time()
                delta_time = time_now - self.last_change_time
                if delta_time.seconds > 10 * 60:
                    if ((time_now.hour >= 22) or (time_now.hour < 6)):
                        self.output_pulse_handler.triggerDoorMovement()
                        self.state = DoorState.CLOSING_COMMAND_SENT
                        self._log_time_door_open(delta_time)
                        self.debug_logger.logText(
                            "AutomaticDoorClosing: new state: CLOSING_COMMAND_SENT")
                        return "-> Automatic door closing trigger sent!"
        return []

    # ------------------------------------------------------------------------------
    def _state_door_auto_close_command_sent(self):
        """
        Process the automatic door closing state.

        Internal state: door is closing triggerd by the supervisor. Wait until
        fully closed

        Returns
        -------
        None.
        """
        current_door_state_open = self.input_handler.getState()
        if not current_door_state_open:
            self.last_change_time = self.time_handler.get_current_time()
            self.state = DoorState.CLOSED
            self.debug_logger.logText("AutomaticDoorClosing: new state: CLOSED")

    # ------------------------------------------------------------------------------
    def _log_time_door_open(self, delta_time):
        """
        Log how long the door is already open.

        Writes an entry to the door log at max every minute.

        Parameters
        ----------
        delta_time : timediff
            Time the door is already open

        Returns
        -------
        None.
        """
        if ((delta_time.seconds % 60) == 0):
            self.debug_logger.logText("Time diff: " + str(delta_time.seconds))

    # ------------------------------------------------------------------------------
    def _eventuallyReenableAutomaticClosing(self):
        """
        Eventually re-enable the automatic closing function.

        Each morning, after the automaitc closing periode ended, at 6 the
        automatic closing is re-enabled.

        Returns
        -------
        None.

        """
        if not self.autoClosingAtNightEnabled:
            time_now = self.time_handler.get_current_time()
            if time_now.hour == 6 and time_now.minute == 0 and time_now.second < 10:
                self.autoClosingAtNightEnabled = True
