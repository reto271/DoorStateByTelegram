#!/bin/bash

# setup environment for virtual python 
python -m venv ~/.venv
source ~/.venv/bin/activate
pip install build
pip install python-telegram
