# Libraries

# Libraries - from Raspberry Pi
from gpiozero import LED

# My modules




# ------------------------------------------------------------------------------
# Output impulse handler
class OutputPulseHandler:

    def __init__(self, debug_logger):
        self.debug_logger = debug_logger
        self.m_output = []
        self.m_requestImpulse = False
        self.m_sendImpulse = False

    def initialize(self, gpioNumber, initValue):
        self.m_output = LED(gpioNumber)
        self.m_output.off()

    def triggerDoorMovement(self):
        self.m_requestImpulse = True
        self.debug_logger.logText('Request door movement')

    def processOutput(self):
        if True == self.m_sendImpulse:
            self.m_output.off()
            self.m_sendImpulse = False

        if True == self.m_requestImpulse:
            self.m_output.on()
            self.m_requestImpulse = False
            self.m_sendImpulse = True
