#!/bin/bash

# change into the project root directory
SCRIPTDIR=$(readlink -f $(dirname "$0"))
pushd "${SCRIPTDIR}" > /dev/null


export GPIOZERO_PIN_FACTORY=lgpio

# setup config & log directories
mkdir -p log
mkdir -p cnfg

# setup default config file
if [ ! -f cnfg/config.txt ]; then
    echo $'[DoorConfig]\nInvertInput = False\n' > cnfg/config.txt
fi
touch cnfg/botId.txt

# get telegram bot and the gpio libs into venv
sudo apt install wget
git clone https://github.com/python-telegram-bot/python-telegram-bot "${SCRIPTDIR}"/../python-telegram-bot

# setup virtual environment and install the dependencies
python3 -m venv env
source env/bin/activate
pip3 install ../python-telegram-bot
pip3 install telepot
#pip3 install rpi.gpio
pip3 install lgpio
pip3 install gpiozero
pip3 install xmlrunner

# Back to the original location
popd > /dev/null
