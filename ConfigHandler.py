# Libraries
import sys
import configparser

# My Modules


# ------------------------------------------------------------------------------
# User handler, adds users to the list and stores them persistent
class ConfigHandler:

    def __init__(self, configFileName, debugLogger = []):
        self.m_configFileName = configFileName
        self.m_debugLogger = debugLogger
        self.m_config = []
        self.__readFile()


    def optionFileFound(self):
        if self.m_config:
            return True
        return False


    def getOption(self, optionName):
        if self.m_config:
            return self.m_config['DoorConfig'][optionName]
        else:
            return []


    def getOptionBool(self, optionName):
        if self.m_config:
            opt = self.m_config['DoorConfig'][optionName]
            return opt == 'True'
        else:
            return []


    def __readFile(self):
        try:
            self.m_config = configparser.ConfigParser()
            readFile = self.m_config.read(self.m_configFileName)
            if [] == readFile:
                self.m_config = []
        except:
            self.__printText('Config file not found: ' + self.m_configFileName)
            self.m_config = []


    def __printText(self, text):
        if self.m_debugLogger:
            self.m_debugLogger.printText(text)
        else:
            print(text)