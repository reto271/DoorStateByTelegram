# Libraries
import time

# Libraries - raspberry pi
from gpiozero import Button

# My modules


# ------------------------------------------------------------------------------
# Boolean input signal encapsulation
class BooleanSignalInput:

    def __init__(self, invertSignal):
        self.m_state = False
        self.m_lastState = False
        self.m_botton = []
        self.m_invertSignal = invertSignal

    def initialize(self, gpioNumber):
        self.m_button = Button(gpioNumber)
        self.sample()
        self.m_lastState = self.m_state

    def sample(self):
        if self.m_button.is_pressed:
            time.sleep(0.2)
            if self.m_button.is_pressed:
                self.m_state = (False == self.m_invertSignal)
        else:
            self.m_state = (True == self.m_invertSignal)

    def isChanged(self):
        didChange = self.m_state != self.m_lastState
        self.m_lastState = self.m_state
        return didChange

    def getState(self):
        return self.m_state
