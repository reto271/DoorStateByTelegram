# Libraries
import datetime

# My modules


# ------------------------------------------------------------------------------
# Logger
class DebugLogger:
    def logMessageCommandReceived(self, firstName, lastName, userName, usrId, text):
        print (str(datetime.datetime.now()) + ' : ' +
               'Request [' + firstName + ' | ' + lastName + ' | ' + userName + '] ' +
               str(usrId) + ' : ' + text)

    def logMessageWithUserId(self, usrId, text):
        print (str(datetime.datetime.now()) + " : " +
               str(usrId) + ' : ' + text)

    def logText(self, text):
        print (str(datetime.datetime.now()) + ' : ' +
               text)

    def logMultiLineText(self, userId, text):
        print (str(datetime.datetime.now()) +
               ' : ' +str(userId) + ' >>>\n' +
               text + '\n<<<\n')