# Libraries
import datetime

# My modules

class TimeHandler:
    """
    Return the actual time.
    Class actually invented to be mocked. The main reason is to mock it and properly test
    the AutomaticDoorClosing class.
    """

    # ------------------------------------------------------------------------------
    def __init__(self, debug_logger):
        self.debug_logger = debug_logger

    # ------------------------------------------------------------------------------
    def get_current_time(self):
        """
        Return the current time.

        Returns
        -------
        TimeObject
            TimeObject represeting the current time.

        """
        return datetime.datetime.now()

    # ------------------------------------------------------------------------------
    def _log_time_door_open(self, delta_time):
        """
        Log how long the door is already open.

        Writes an entry to the door log at max every minute.

        Parameters
        ----------
        delta_time : timediff
            Time the door is already open

        Returns
        -------
        None.
        """
        if ((delta_time.seconds % 60) == 0):
            self.debug_logger.logText("Time diff: " + str(delta_time.seconds))
