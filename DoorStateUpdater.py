# Libraries
import os
import time
import datetime
import telepot

# My modules
import myUtils
import ProjectVersion
from UserListHandler import UserListHandler
from DoorStatistics import DoorStatistics
from ConfigHandler import ConfigHandler
from BooleanSignalInput import BooleanSignalInput
from OutputPulseHandler import OutputPulseHandler
from AutomaticDoorClosing import AutomaticDoorClosing
from AccesRequestHandler import AccesRequestHandler
from DebugLogger import DebugLogger
from TimeHandler import TimeHandler


# ------------------------------------------------------------------------------
def startupInformation(userId, bot = []):
    """
    Print the startup information.

    Parameters
    ----------
    userId : string
        User identifier of the bot.
    bot : telepot.Bot, optional
        Reference to the bot.

    Returns
    -------
    None.

    """
    helpText = str('\nReboot...\n\n' +
                   'Garage Door Controller\n' +
                   ProjectVersion.getVersionNumber() +
                   '\n(c) by reto271\n')
    m_debugLogger.logMultiLineText(userId, helpText)
    if bot:
        bot.sendMessage(userId, helpText)


# ------------------------------------------------------------------------------
def usageInformation(bot, userId):
    """
    Print software infos.

    Parameters
    ----------
    bot : telepot.Bot
        The telegram bot.
    userId : string
        User ID of the user requesting the info.

    Returns
    -------
    None.

    """
    helpText = str('Garage Door Controller - ' +
                   ProjectVersion.getVersionNumber() +
                   '\n\nSend the following messages to the bot:\n' +
                   '   T: to get the current TIME.\n' +
                   # '  Reg: to REGISTER yourself.
                   #    You will get state updates.\n' +
                   '   G: GET the current door state.\n' +
                   '   C: CLOSE the door.\n' +
                   '   O: OPEN the door.\n' +
                   '   E: ENABLE notifications.\n' +
                   '   D: DISABLE notifications.\n' +
                   '   S: STATUS dump.\n' +
                   '   P: PAUSE automatic door closing today.\n' +
                   '   A: AUTOMATIC door closing enable.\n' +
                   '   H: print this HELP.\n' +
                   '   Hw: print the HW version of the Raspberry Pi.\n' +
                   '\n(c) by reto271\n')
    m_debugLogger.logMultiLineText(userId, helpText)
    if '' != bot:
        bot.sendMessage(userId, helpText)


# ------------------------------------------------------------------------------
# Message handler for the bot
def handle(msg):
    userId = getIntKey2(msg, 'chat', 'id', -1)
    command = getStringKey1(msg, 'text', '-')
    firstName = getStringKey2(msg, 'from', 'first_name', 'NoFirstName')
    lastName = getStringKey2(msg, 'from', 'last_name', 'NoLastName')
    userName = getStringKey2(msg, 'from', 'username', 'NoUserName')

    m_debugLogger.logText('-------------------------------------------')
    # m_debugLogger.logText(str(msg))
    m_debugLogger.logMessageCommandReceived(firstName, lastName,
                                            userName, userId, command)

    # -----
    # The only accessible command if the user is not registered
    if 'Reg' == command:
        m_accessRequestHandler.requestPermission(firstName, lastName,
                                                 userName, userId)

    # -----
    # User commands
    elif command == 'T':
        if m_userAccessList.isUserRegistered(userId):
            bot.sendMessage(userId, str(datetime.datetime.now()))

    elif command == 'G':
        if m_userAccessList.isUserRegistered(userId):
            if m_doorStateInput.getState():
                m_debugLogger.logText('Door open')
                bot.sendMessage(userId, 'Door state: open')
            else:
                m_debugLogger.logText('Door closed')
                bot.sendMessage(userId, 'Door state: closed')

    elif command == 'H':
        if m_userAccessList.isUserRegistered(userId):
            usageInformation(bot, userId)

    elif command == 'C':
        if m_userAccessList.isUserRegistered(userId):
            if m_doorStateInput.getState():
                bot.sendMessage(userId, 'Door closing...')
                m_doorMovementOutput.triggerDoorMovement()
            else:
                bot.sendMessage(userId, 'Door is already closed.')
                m_debugLogger.logText('Door is already closed.')

    elif command == 'O':
        if m_userAccessList.isUserRegistered(userId):
            if not m_doorStateInput.getState():
                bot.sendMessage(userId, 'Door opening...')
                m_doorMovementOutput.triggerDoorMovement()
            else:
                bot.sendMessage(userId, 'Door is already open.')
                m_debugLogger.logText('Door is already open.')

    elif 'E' == command:
        if m_userAccessList.isUserRegistered(userId):
            text = 'Notifications enabled'
            m_userNotificationList.addUser(userId)
            m_userNotificationList.storeList()
            bot.sendMessage(userId, text)
            m_debugLogger.logMessageWithUserId(userId, text)

    elif 'D' == command:
        if m_userAccessList.isUserRegistered(userId):
            text = 'Notifications disabled'
            m_userNotificationList.removeUser(userId)
            m_userNotificationList.storeList()
            bot.sendMessage(userId, text)
            m_debugLogger.logMessageWithUserId(userId, text)

    elif 'Hw' == command:
        if m_userAccessList.isUserRegistered(userId):
            hwVersion = getRaspberryPi_HW_Version()
            bot.sendMessage(userId, hwVersion)
            m_debugLogger.logMessageWithUserId(userId, hwVersion)

    elif 'S' == command:
        if m_userAccessList.isUserRegistered(userId):
            m_doorStats.dumpState(userId)

    elif 'P' == command:
        if m_userAccessList.isUserRegistered(userId):
            m_automatic_door_closing.disableAutoClosingToday()
            text = 'Automatic closing disabled'
            bot.sendMessage(userId, text)
            m_debugLogger.logMessageWithUserId(userId, text)

    elif 'A' == command:
        if m_userAccessList.isUserRegistered(userId):
            m_automatic_door_closing.enableAutoClosingToday()
            text = 'Automatic closing enabled'
            bot.sendMessage(userId, text)
            m_debugLogger.logMessageWithUserId(userId, text)

    # -----
    # Admin commands
    elif 'Y' == command[0]:
        if m_accessRequestHandler.isAdmin(userId):
            m_accessRequestHandler.ackNewUser(command[2:])

    elif 'N' == command[0]:
        if m_accessRequestHandler.isAdmin(userId):
            m_accessRequestHandler.rejectNewUser(command[2:])

    elif 'Pr' == command:
        if m_accessRequestHandler.isAdmin(userId):
            m_accessRequestHandler.showPendingRequests()

    else:
        if m_userAccessList.isUserRegistered(userId):
            bot.sendMessage(userId, 'Command not supported.')
            m_debugLogger.logText('Command not supported.')


# ------------------------------------------------------------------------------
# Extract string from first level key
def getStringKey1(testDict, keyName, defaultString):
    strValue = defaultString
    if keyName in testDict:
        strValue = testDict[keyName]
    return strValue


# ------------------------------------------------------------------------------
# Extract string from second level key
def getStringKey2(testDict, keyName, keySubName, defaultString):
    strValue = defaultString
    if keyName in testDict:
        if keySubName in testDict[keyName]:
            strValue = testDict[keyName][keySubName]
    return strValue


# ------------------------------------------------------------------------------
# Extract int from first level key
def getIntKey1(testDict, keyName, defaultValue):
    intValue = defaultValue
    if keyName in testDict:
        intValue = myUtils.tryInt(testDict[keyName], defaultValue)
    return intValue


# ------------------------------------------------------------------------------
# Extract int from second level key
def getIntKey2(testDict, keyName, keySubName, defaultValue):
    intValue = defaultValue
    if keyName in testDict:
        if keySubName in testDict[keyName]:
            intValue = myUtils.tryInt(testDict[keyName][keySubName], defaultValue)
    return intValue


# ------------------------------------------------------------------------------
# Check and eventually create directory
def createDirectory(dirName):
    if not os.path.exists(dirName):
        os.makedirs(dirName)


# ------------------------------------------------------------------------------
# Periodically polls the inputs and sends status updates
def sendDoorStateUpdate():
    doorNotification = []
    if m_doorStateInput.getState():
        doorNotification = '-> Door state: open'
    else:
        doorNotification = '-> Door state: closed'
    sendNotification(doorNotification)


# ------------------------------------------------------------------------------
# Send notification text to all users in the notification list.
def sendNotification(notification_text):
    if m_userNotificationList.isListEmpty():
        m_debugLogger.logText('No registered users to notify')
    else:
        userList = m_userNotificationList.getUserList()
        for userId in userList:
            try:
                bot.sendMessage(userId, notification_text)
            except:
                m_debugLogger.logMessageWithUserId(userId, '*******')
                m_debugLogger.logMessageWithUserId(
                    userId, 'COULD NOT SEND NOTIFICATION')
                m_debugLogger.logMessageWithUserId(userId, notification_text)
                m_debugLogger.logMessageWithUserId(userId, '*******')
            m_debugLogger.logMessageWithUserId(userId, notification_text)


# ------------------------------------------------------------------------------
# Reads the telegram Id of this bot from cnfg/botId.txt
def readTelegramId():
    try:
        with open('./cnfg/botId.txt', 'r') as idfile:
            myId = idfile.read().rstrip()
    except IOError:
        myId = ''
        m_debugLogger.logText('File "cnfg/botId.txt" not found.')
    return myId


# ------------------------------------------------------------------------------
# Get Raspberry Pi HW Info from the cpuinfo file
def getRaspberryPi_HW_Version():
    myHW_Info = "-"
    try:
        f = open('/proc/cpuinfo', 'r')
        for line in f:
            if line[0:5] == 'Model':
                length = len(line)
                myHW_Info = line[9:length-1]
        f.close()
    except:
        myHW_Info = "unknown"
    return myHW_Info


# ------------------------------------------------------------------------------
# Main program

createDirectory('log')
createDirectory('cnfg')

m_debugLogger = DebugLogger()
m_doorStats = []

m_cnfgHdl = ConfigHandler('cnfg/config.txt', m_debugLogger)
if not m_cnfgHdl.optionFileFound():
    m_debugLogger.logText('Config file not found (cnfg/config.txt)')
else:
    m_telegramId = readTelegramId()

    m_userAccessList = UserListHandler(m_debugLogger)
    m_userAccessList.initialize('./cnfg/registeredIds.txt')
    m_userAccessList.loadList()
    # m_userAccessList.printList()

    m_userNotificationList = UserListHandler(m_debugLogger)
    m_userNotificationList.initialize('./cnfg/notificationIds.txt')
    m_userNotificationList.loadList()
    # m_userNotificationList.printList()

    # Use GPIO 23
    m_doorStateInput = BooleanSignalInput(
        m_cnfgHdl.getOptionBool('InvertInput'))
    m_doorStateInput.initialize(23)

    # Use GPIO 24
    m_doorMovementOutput = OutputPulseHandler(m_debugLogger)
    m_doorMovementOutput.initialize(24, False)

    if '' == m_telegramId:
        m_debugLogger.logText(
            'Internal telegram id not found. Create a file ' +
            '"cnfg/botId.txt" containing the ID of the bot.')
    else:
        bot = telepot.Bot(m_telegramId)
        m_time_handler = TimeHandler(m_debugLogger)
        m_automatic_door_closing = AutomaticDoorClosing(m_doorStateInput,
                                                        m_doorMovementOutput,
                                                        m_time_handler,
                                                        m_debugLogger)

        m_accessRequestHandler = AccesRequestHandler(m_debugLogger,
                                                     m_userAccessList,
                                                     m_userNotificationList,
                                                     bot)
        m_accessRequestHandler.initialize()

        m_doorStats = DoorStatistics(bot, m_automatic_door_closing, m_debugLogger)
        bot.message_loop(handle)

        userList = m_userNotificationList.getUserList()
        for userId in userList:
            startupInformation(userId, bot)
            m_doorStats.dumpState(userId)
        startupInformation(0)          # To the log, if there is no observer.
        m_doorStats.dumpState()

        while 1:
            time.sleep(1)
            m_doorStateInput.sample()
            m_doorMovementOutput.processOutput()

            closing_request = m_automatic_door_closing.process()
            if [] != closing_request:
                sendNotification(closing_request)

            if m_doorStateInput.isChanged():
                sendDoorStateUpdate()
                m_doorStats.addDoorMovement()
            m_doorStats.run()
