#!/usr/bin/env python3
# encoding: utf-8

import datetime
import unittest
# from unittest.mock import MagicMock
from unittest.mock import Mock
# DUT:
from AutomaticDoorClosing import AutomaticDoorClosing
from AutomaticDoorClosing import DoorState
# from BooleanSignalInput import BooleanSignalInput -> mock it

class Test_AutomaticDoorClosing(unittest.TestCase):
    def __init__(self, *args, **kwargs):
        unittest.TestCase.__init__(self, *args, **kwargs)

    def setUp(self):
        pass

    def tearDown(self):
        pass

    def _setup_DUT(self, initial_time, door_state = DoorState.OPEN):
        # Create mock objects
        self.mock_input_handler = Mock()
        self.mock_output_pulse_handler = Mock()
        self.mock_time_handler = Mock()
        self.mock_debug_logger = Mock()

        # Mock time
        print("\nInitial Time", initial_time)
        self.mock_time_handler.get_current_time.return_value = initial_time

        # Mock input (door state)
        self.mock_input_handler.getState.return_value = (door_state == door_state.OPEN)

        # Create device unter test
        self.dut = AutomaticDoorClosing(self.mock_input_handler,
                                         self.mock_output_pulse_handler,
                                         self.mock_time_handler,
                                         self.mock_debug_logger)



    def test_process_one_step(self):
        self._setup_DUT(datetime.datetime(2023, 3, 3, 10, 0, 0, 0))
        self.mock_output_pulse_handler.triggerDoorMovement.assert_not_called()
        self.dut.process()
        self.assertTrue(self.dut.isAutoClosingEnabled())

    def test_keep_door_open(self):
        self._setup_DUT(datetime.datetime(2023, 3, 3, 10, 0, 0, 0))
        self.mock_output_pulse_handler.triggerDoorMovement.assert_not_called()
        self.mock_time_handler.get_current_time.return_value = datetime.datetime(2023, 3, 3, 10, 5, 0, 0)
        self.dut.process()
        self.assertTrue(self.dut.isAutoClosingEnabled())

    def test_keep_door_open_wrong_day_time(self):
        self._setup_DUT(datetime.datetime(2023, 3, 3, 10, 0, 0, 0))
        self.mock_output_pulse_handler.triggerDoorMovement.assert_not_called()
        self.mock_time_handler.get_current_time.return_value = datetime.datetime(2023, 3, 3, 10, 2, 0, 0)
        self.dut.process()
        self.mock_time_handler.get_current_time.return_value = datetime.datetime(2023, 3, 3, 10, 5, 0, 0)
        self.dut.process()
        self.mock_time_handler.get_current_time.return_value = datetime.datetime(2023, 3, 3, 10, 10, 0, 0)
        self.dut.process()
        self.mock_time_handler.get_current_time.return_value = datetime.datetime(2023, 3, 3, 10, 11, 0, 0)
        self.dut.process()
        self.assertTrue(self.dut.isAutoClosingEnabled())

    def test_expect_door_closing_command(self):
        # Construct an other dut at a different initial time (namely at night)
        self._setup_DUT(datetime.datetime(2023, 3, 3, 23, 0, 0, 0))

        self.mock_output_pulse_handler.triggerDoorMovement.assert_not_called()
        self.mock_time_handler.get_current_time.return_value = datetime.datetime(2023, 3, 3, 23, 0, 0, 0)
        self.dut.process()
        self.mock_time_handler.get_current_time.return_value = datetime.datetime(2023, 3, 3, 23, 2, 0, 0)
        self.dut.process()
        self.mock_time_handler.get_current_time.return_value = datetime.datetime(2023, 3, 3, 23, 5, 0, 0)
        self.dut.process()
        self.mock_time_handler.get_current_time.return_value = datetime.datetime(2023, 3, 3, 23, 10, 0, 0)
        self.dut.process()

        # Not called until here
        self.mock_output_pulse_handler.triggerDoorMovement.assert_not_called()

        # Now the closing must be triggered
        self.mock_time_handler.get_current_time.return_value = datetime.datetime(2023, 3, 3, 23, 10, 1, 0)
        self.dut.process()
        # it must be called in the process call right before
        self.mock_output_pulse_handler.triggerDoorMovement.assert_called_once()
        self.assertTrue(self.dut.isAutoClosingEnabled())


    def test_door_sia_already_closed_not_trigger(self):
        self._setup_DUT(datetime.datetime(2023, 3, 3, 23, 0, 0, 0), DoorState.CLOSED)

        self.mock_output_pulse_handler.triggerDoorMovement.assert_not_called()
        self.mock_time_handler.get_current_time.return_value = datetime.datetime(2023, 3, 3, 23, 0, 0, 0)
        self.dut.process()
        self.mock_time_handler.get_current_time.return_value = datetime.datetime(2023, 3, 3, 23, 2, 0, 0)
        self.dut.process()
        self.mock_time_handler.get_current_time.return_value = datetime.datetime(2023, 3, 3, 23, 5, 0, 0)
        self.dut.process()
        self.mock_time_handler.get_current_time.return_value = datetime.datetime(2023, 3, 3, 23, 10, 0, 0)
        self.dut.process()

        # Not called until here
        self.mock_output_pulse_handler.triggerDoorMovement.assert_not_called()

        # Now the closing must be triggered
        self.mock_time_handler.get_current_time.return_value = datetime.datetime(2023, 3, 3, 23, 10, 1, 0)
        self.dut.process()

        # Should not close the door. It is already close.
        self.mock_output_pulse_handler.triggerDoorMovement.assert_not_called()
        self.assertTrue(self.dut.isAutoClosingEnabled())

    def test_no_pulse_door_already_closed(self):
        # Simulate already closed door
        self._setup_DUT(datetime.datetime(2023, 3, 3, 23, 0, 0, 0), DoorState.CLOSED)

        self.mock_output_pulse_handler.triggerDoorMovement.assert_not_called()
        self.mock_time_handler.get_current_time.return_value = datetime.datetime(2023, 3, 3, 23, 0, 0, 0)
        self.dut.process()
        self.mock_time_handler.get_current_time.return_value = datetime.datetime(2023, 3, 3, 23, 11, 0, 0)
        self.dut.process()
        self.assertTrue(self.dut.isAutoClosingEnabled())

    def test_door_opens_at_2am_and_closes_automatically_ten_min_later(self):
        self._setup_DUT(datetime.datetime(2023, 3, 3, 1, 0, 0, 0), DoorState.CLOSED)

        # no automatic trigger so far
        self.mock_output_pulse_handler.triggerDoorMovement.assert_not_called()

        # process over time
        self.mock_time_handler.get_current_time.return_value = datetime.datetime(2023, 3, 3, 1, 10, 0, 0)
        self.dut.process()
        self.mock_time_handler.get_current_time.return_value = datetime.datetime(2023, 3, 3, 1, 59, 0, 0)
        self.dut.process()

        # door opens at 2pm
        self.mock_input_handler.getState.return_value = True
        self.mock_time_handler.get_current_time.return_value = datetime.datetime(2023, 3, 3, 2, 0, 0, 0)
        self.dut.process()

        self.mock_time_handler.get_current_time.return_value = datetime.datetime(2023, 3, 3, 2, 10, 0, 0)
        self.dut.process()
        self.mock_output_pulse_handler.triggerDoorMovement.assert_not_called()

        # trigger door auto closing
        self.mock_time_handler.get_current_time.return_value = datetime.datetime(2023, 3, 3, 2, 10, 1, 0)
        self.dut.process()
        self.mock_output_pulse_handler.triggerDoorMovement.assert_called_once()
        self.assertTrue(self.dut.isAutoClosingEnabled())

    def test_door_opens_at_2pm_and_therefore_no_automatically_close_triggered(self):
        self._setup_DUT(datetime.datetime(2023, 3, 3, 13, 0, 0, 0), DoorState.CLOSED)

        # no automatic trigger so far
        self.mock_output_pulse_handler.triggerDoorMovement.assert_not_called()

        # process over time
        self.mock_time_handler.get_current_time.return_value = datetime.datetime(2023, 3, 3, 13, 10, 0, 0)
        self.dut.process()
        self.mock_time_handler.get_current_time.return_value = datetime.datetime(2023, 3, 3, 13, 59, 0, 0)
        self.dut.process()

        # door opens at 2pm
        self.mock_input_handler.getState.return_value = True
        self.mock_time_handler.get_current_time.return_value = datetime.datetime(2023, 3, 3, 14, 0, 0, 0)
        self.dut.process()

        self.mock_time_handler.get_current_time.return_value = datetime.datetime(2023, 3, 3, 14, 10, 0, 0)
        self.dut.process()
        self.mock_output_pulse_handler.triggerDoorMovement.assert_not_called()

        # trigger door auto closing
        self.mock_time_handler.get_current_time.return_value = datetime.datetime(2023, 3, 3, 14, 10, 1, 0)
        self.dut.process()
        self.mock_output_pulse_handler.triggerDoorMovement.assert_not_called()
        self.assertTrue(self.dut.isAutoClosingEnabled())

    def test_no_door_closing_command_because_temporary_disabled(self):
        # Construct an other dut at a different initial time (namely at night)
        self._setup_DUT(datetime.datetime(2023, 3, 3, 23, 0, 0, 0))
        self.dut.disableAutoClosingToday() # !!!

        self.mock_output_pulse_handler.triggerDoorMovement.assert_not_called()
        self.mock_time_handler.get_current_time.return_value = datetime.datetime(2023, 3, 3, 23, 0, 0, 0)
        self.dut.process()
        self.mock_time_handler.get_current_time.return_value = datetime.datetime(2023, 3, 3, 23, 2, 0, 0)
        self.dut.process()
        self.mock_time_handler.get_current_time.return_value = datetime.datetime(2023, 3, 3, 23, 5, 0, 0)
        self.dut.process()
        self.mock_time_handler.get_current_time.return_value = datetime.datetime(2023, 3, 3, 23, 10, 0, 0)
        self.dut.process()

        # Not called until here
        self.mock_output_pulse_handler.triggerDoorMovement.assert_not_called()

        # Now the closing must be triggered
        self.mock_time_handler.get_current_time.return_value = datetime.datetime(2023, 3, 3, 23, 10, 1, 0)
        self.dut.process()
        # it must be called in the process call right before
        self.mock_output_pulse_handler.triggerDoorMovement.assert_not_called()
        self.assertFalse(self.dut.isAutoClosingEnabled())

    def test_automatic_door_closing_reenabled_after_a_day(self):
        self._setup_DUT(datetime.datetime(2023, 3, 3, 23, 0, 0, 0))
        self.dut.disableAutoClosingToday() # !!!
        self.assertFalse(self.dut.isAutoClosingEnabled())
        self.mock_time_handler.get_current_time.return_value = datetime.datetime(2023, 3, 4, 5, 59, 59, 0)
        self.dut.process()
        self.assertFalse(self.dut.isAutoClosingEnabled())
        # At 6 o'clock it is re-enabled
        self.mock_time_handler.get_current_time.return_value = datetime.datetime(2023, 3, 4, 6, 0, 0, 0)
        self.dut.process()
        self.assertTrue(self.dut.isAutoClosingEnabled())

if __name__ == '__main__':
    import xmlrunner
    unittest.main(testRunner=xmlrunner.XMLTestRunner(output='test-reports'))
    #unittest.main()  # Calling from the command line invokes all tests
