# Libraries


def getVersionNumber():
    version_file = 'VersionNumber.txt'
    try:
        with open(version_file, 'r') as versionFile:
            versionNumber = versionFile.read().rstrip()
            versionFile.close()
            return versionNumber
    except:
        print('File ' + str(version_file) + ' not found.')
        return ''