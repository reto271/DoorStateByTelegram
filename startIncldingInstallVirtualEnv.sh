#!/bin/bash

# Change into the project root directory
SCRIPTDIR=$(readlink -f $(dirname "$0"))
pushd "${SCRIPTDIR}" > /dev/null


export GPIOZERO_PIN_FACTORY=lgpio
source env/bin/activate
bash ./startDoorBot_internal.sh


# Back to the original location
popd > /dev/null
